﻿using System;
using System.Collections.Generic;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;
using FluentAssertions;
using Moq;
using Xunit;

namespace Emprevo.Coding.Test.UnitTests
{
    public class BaseCalculateEngineTests
    {
        [Fact]
        public void Given_EntryDate_GreaterThan_ExitDate_Results_Exception()
        {
            //Arrange            
            var mockNightRateCondition = new Mock<IRateCondition>();
            var rateConditions = new List<IRateCondition>();
            rateConditions.Add(mockNightRateCondition.Object);
            var startDateTime = new DateTime(2018, 2, 5, 15, 30, 0);
            var endDateTime = new DateTime(2018, 2, 3, 6, 30, 0);
            var calculateEngine = new CarParkCalculateEngine(rateConditions);
            //Act 
            Action action = new Action(() =>
              {
                  calculateEngine.GetCarParkingPriceDetails(startDateTime, endDateTime);
              });

            //Assert
            action.Should().Throw<Exception>().WithMessage("Exitdate must be greater than Entrydate");
        }

        [Fact]
        public void Given_EntryDate_And_ExitDate_Return_Rate_Results()
        {
            //Arrange            
            var mockNightRateCondition = new Mock<IRateCondition>();
            mockNightRateCondition.Setup(c => c.Condition(It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(new WeekendRate());
            var rateConditions = new List<IRateCondition>();
            rateConditions.Add(mockNightRateCondition.Object);
            var startDateTime = new DateTime(2018, 2, 3, 15, 30, 0);
            var endDateTime = new DateTime(2018, 2, 3, 19, 30, 0);
            var calculateEngine = new CarParkCalculateEngine(rateConditions);
            //Act 
            var carParkingFeeDetails= calculateEngine.GetCarParkingPriceDetails(startDateTime, endDateTime);
            //Assert
            carParkingFeeDetails.Should().NotBeNull();
            carParkingFeeDetails.Should().BeOfType<CarParkingFeeDetails>();
            carParkingFeeDetails.Rate.Should().BeOfType<WeekendRate>();
            carParkingFeeDetails.Rate.Fee.Equals(10.0m);
            carParkingFeeDetails.Rate.Type.Equals(RateType.FlatRate);
        }
    }
}

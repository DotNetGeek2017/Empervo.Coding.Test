﻿using System;
using Emprevo.Coding.Test.Extensions;
using FluentAssertions;
using Xunit;

namespace Emprevo.Coding.Test.UnitTests.DateTimeExtensions
{
    public class DateTimeExtensionsTests
    {
        [Fact]
        public void Given_DateTime_IsInBetween_StartAndEndDateTime()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 2, 18, 29, 0);
            var startDateTime = new DateTime(2018, 2, 2, 15, 30, 0);
            var endDateTime = new DateTime(2018, 2, 3, 6, 30, 0);
            //Act
           var result= dateTime.IsTimeInBetween(startDateTime, endDateTime);

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void Given_DateTime_IsNotInBetween_StartAndEndDateTime()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 2, 12, 29, 0);
            var startDateTime = new DateTime(2018, 2, 2, 15, 30, 0);
            var endDateTime = new DateTime(2018, 2, 3, 6, 30, 0);
            //Act
            var result = dateTime.IsTimeInBetween(startDateTime, endDateTime);

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void Given_DateTime_IsTimeBefore_StartDateTime()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 2, 12, 29, 0);
            var startDateTime = new DateTime(2018, 2, 2, 15, 30, 0);
             //Act
            var result = dateTime.IsTimeBefore(startDateTime);

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void Given_DateTime_IsNotTimeBefore_StartDateTime()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 2, 16, 29, 0);
            var startDateTime = new DateTime(2018, 2, 2, 15, 30, 0);
            //Act
            var result = dateTime.IsTimeBefore(startDateTime);

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void Given_DateTime_FallsOn_Weekend()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 3, 16, 29, 0);
            //Act
            var result = dateTime.IsWeekend();

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void Given_DateTime_DoesNotFallsOn_Weekend()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 2, 23, 29, 0);
            //Act
            var result = dateTime.IsWeekend();

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void Given_DateTime_FallsOn_Weekday()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 2, 16, 29, 0);
            //Act
            var result = dateTime.IsWeekDay();

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void Given_DateTime_DoesNotFallsOn_Weekday()
        {
            //Arrange
            var dateTime = new DateTime(2018, 2, 3, 23, 29, 0);
            //Act
            var result = dateTime.IsWeekDay();

            //Assert
            result.Should().BeFalse();
        }


    }
}

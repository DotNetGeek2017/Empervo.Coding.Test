﻿using System;
using System.Collections.Generic;
using Emprevo.Coding.Test.Conditions;
using Emprevo.Coding.Test.Interface;
using FluentAssertions;
using Moq;
using Xunit;

namespace Emprevo.Coding.Test.UnitTests.RateConditions
{
    public class EarlyBirdRateConditionTests
    {
        [Fact]
        public void Given_EntryAndExitDateTime_ConditionFails_Returns_NullValue()
        {
            //Arrange
            var mockEarlyBirdRate = new Mock<IRate>();           
            var earlyBirdRateCondition = new EarlyBirdRateCondition(mockEarlyBirdRate.Object);
            var startDateTime = new DateTime(2018, 2, 2, 10, 45, 0);
            //Act
            var result=earlyBirdRateCondition.Condition(startDateTime, startDateTime.AddDays(1));
            //Assert
            result.Should().BeNull();
        }

        [Theory, MemberData(nameof(EarlyBirdDateTimes))]
        public void Given_EntryAndExitDateTime_Returns_EarlyBirdRate(DateTime entryDateTime,DateTime exitDateTime)
        {
            //Arrange
            var mockEarlyBirdRate = new Mock<IRate>();
            mockEarlyBirdRate.Setup(c => c.Fee).Returns(6.50m);
            mockEarlyBirdRate.Setup(c => c.Name).Returns("EarlyBirdRate");
            mockEarlyBirdRate.Setup(c => c.Type).Returns(RateType.FlatRate);

            var earlyBirdRateCondition = new EarlyBirdRateCondition(mockEarlyBirdRate.Object);
            //Act
            var result = earlyBirdRateCondition.Condition(entryDateTime, exitDateTime);
            //Assert
            result.Should().NotBeNull();
            result.Should().Equals(6.50m);
            result.Type.Should().Equals(RateType.FlatRate);
            result.Name.Should().Equals("EarlyBirdRate");
        }

        public static IEnumerable<Object[]> EarlyBirdDateTimes
        {
            get
            {
                return new[]
                {
                new object[] { new DateTime(2018, 02, 02, 6, 0, 0), new DateTime(2018, 02, 02, 15, 30, 0) },
                new object[] { new DateTime(2018, 02, 02, 8, 45, 0), new DateTime(2018, 02, 02, 23, 30, 0) },
                new object[] { new DateTime(2018, 02, 02, 9, 0, 0), new DateTime(2018, 02, 02, 19, 30, 0) },
            };
            }
        }

    }
}

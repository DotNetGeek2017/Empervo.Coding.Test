﻿using System;
using System.Collections.Generic;
using Emprevo.Coding.Test.Conditions;
using Emprevo.Coding.Test.Interface;
using FluentAssertions;
using Moq;
using Xunit;

namespace Emprevo.Coding.Test.UnitTests.RateConditions
{
    public class NightRateConditionTests
    {
        [Fact]
        public void Given_EntryAndExitDateTime_Returns_NullValue()
        {
            //Arrange
            var mockNightRate = new Mock<IRate>();
            mockNightRate.Setup(c => c.Fee).Returns(13.50m);
            mockNightRate.Setup(c => c.Type).Returns(RateType.FlatRate);
            mockNightRate.Setup(c => c.Name).Returns("NightRate");
            var startDateTime = new DateTime(2018, 2, 2, 10, 45, 0);

            var nightRate = new NightRateCondition(mockNightRate.Object);
            //Act
            var result=nightRate.Condition(startDateTime, startDateTime.AddDays(1));
            //Assert
            result.Should().BeNull();
        }

        [Theory]
        [MemberData(nameof(NightBirdDateTimes))]
        public void Given_EntryAndExitDateTime_Returns_NightRate(DateTime entryDateTime, DateTime exitDateTime)
        {
            //Arrange
            var mockNightRate = new Mock<IRate>();
            mockNightRate.Setup(c => c.Fee).Returns(13.50m);
            mockNightRate.Setup(c => c.Type).Returns(RateType.FlatRate);
            mockNightRate.Setup(c => c.Name).Returns("NightRate");

            var nightRate = new NightRateCondition(mockNightRate.Object);
            //Act
            var result = nightRate.Condition(entryDateTime, exitDateTime);
            //Assert
            result.Should().NotBeNull();
            result.Fee.Should().Equals(13.50m);
            result.Type.Should().Equals(RateType.FlatRate);
            result.Name.Should().Equals("NightRate");
        }

        public static IEnumerable<Object[]> NightBirdDateTimes
        {
            get
            {
                return new[]
                {
                new object[] { new DateTime(2018, 02, 02, 18, 0, 0), new DateTime(2018, 02, 02, 23, 30, 0) },
                new object[] { new DateTime(2018, 02, 01, 23, 59, 0), new DateTime(2018, 02, 02, 3, 30, 0) },
                new object[] { new DateTime(2018, 02, 01, 20, 0, 0), new DateTime(2018, 02, 02, 6, 0, 0) },
            };
            }
        }
    }
}

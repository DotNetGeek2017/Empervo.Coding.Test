﻿using System;
using System.Collections.Generic;
using Emprevo.Coding.Test.Conditions;
using Emprevo.Coding.Test.Interface;
using FluentAssertions;
using Moq;
using Xunit;

namespace Emprevo.Coding.Test.UnitTests.RateConditions
{
    public class WeekendRateConditionTests
    {
        [Fact]
        public void Given_EntryAndExitDateTime_Returns_NullValue()
        {
            //Arrange
            var mockNightRate = new Mock<IRate>();
            mockNightRate.Setup(c => c.Fee).Returns(12.50m);
            mockNightRate.Setup(c => c.Type).Returns(RateType.FlatRate);
            mockNightRate.Setup(c => c.Name).Returns("WeekendRate");
            var startDateTime = new DateTime(2018, 2, 2, 10, 45, 0);

            var weekendRate = new WeekendRateCondition(mockNightRate.Object);
            //Act
            var result = weekendRate.Condition(startDateTime, startDateTime.AddDays(1));
            //Assert
            result.Should().BeNull();
        }

        [Theory]
        [MemberData(nameof(WeekendDateTimes))]
        public void Given_EntryAndExitDateTime_Returns_WeekendRate(DateTime entryDateTime, DateTime exitDateTime)
        {
            //Arrange
            var mockWeekendRate = new Mock<IRate>();
            mockWeekendRate.Setup(c => c.Fee).Returns(12.50m);
            mockWeekendRate.Setup(c => c.Type).Returns(RateType.FlatRate);
            mockWeekendRate.Setup(c => c.Name).Returns("WeekendRate");

            var weekendRate = new WeekendRateCondition(mockWeekendRate.Object);
            //Act
            var result = weekendRate.Condition(entryDateTime, exitDateTime);
            //Assert
            result.Should().NotBeNull();
            result.Fee.Should().Equals(12.50m); ;
            result.Type.Should().Equals(RateType.FlatRate);
            result.Name.Should().Equals("WeekendRate");
        }

        public static IEnumerable<Object[]> WeekendDateTimes
        {
            get
            {
                return new[]
                {
                new object[] { new DateTime(2018, 02, 03, 18, 0, 0), new DateTime(2018, 02, 03, 23, 30, 0) },
                new object[] { new DateTime(2018, 02, 03, 23, 59, 0), new DateTime(2018, 02, 04, 3, 30, 0) },
                new object[] { new DateTime(2018, 02, 04, 20, 0, 0), new DateTime(2018, 02, 04, 23, 59, 0) },
            };
            }
        }
    }
}

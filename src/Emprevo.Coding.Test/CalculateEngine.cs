﻿using System;
using System.Collections.Generic;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;

namespace Emprevo.Coding.Test
{
    public class CarParkCalculateEngine : BaseCalculateEngine
    {
        public CarParkCalculateEngine(IEnumerable<IRateCondition> ratesConditions) :base(ratesConditions)
        {
        }

        public override CarParkingFeeDetails GetCarParkingPriceDetails(DateTime entryDateTime,DateTime exitDateTime)
        {
            return base.GetCarParkingPriceDetails(entryDateTime,exitDateTime);
        }
    }
}


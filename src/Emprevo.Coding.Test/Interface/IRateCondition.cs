﻿using System;

namespace Emprevo.Coding.Test.Interface
{
    public interface IRateCondition
    {
        IRate Condition(DateTime entryDateTime, DateTime exitDateTime);

    }
}

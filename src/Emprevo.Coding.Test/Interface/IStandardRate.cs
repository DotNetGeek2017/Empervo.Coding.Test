﻿
using System;

namespace Emprevo.Coding.Test.Interface
{
    public interface IStandardRate: IRate
    {
        decimal TotalPrice(DateTime entryDateTime,DateTime exitDateTime);

        decimal MaxRatePerDay { get; }
    }
}

﻿using System;
using Emprevo.Coding.Test.Extensions;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;

namespace Emprevo.Coding.Test.Conditions
{
    public class WeekendRateCondition : IRateCondition
    {
        protected readonly IRate _rate;

        public WeekendRateCondition(IRate rate)
        {
            _rate = rate;
            _rate.Register(this);
        }

        public IRate Condition(DateTime entryDateTime, DateTime exitDateTime)
        {
            if(!(entryDateTime.IsWeekend() && exitDateTime.IsWeekend()))
                return null;

            if (exitDateTime.BeforeMidNight())
                return _rate;

            return null;
        }
    }
}

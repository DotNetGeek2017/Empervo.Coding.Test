﻿using System;
using Emprevo.Coding.Test.Extensions;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;

namespace Emprevo.Coding.Test.Conditions
{
    public class EarlyBirdRateCondition : IRateCondition
    {
        protected readonly IRate _rate;
        public EarlyBirdRateCondition(IRate rate)
        {
            _rate = rate;
            _rate.Register(this);
        }

        public IRate Condition(DateTime entryDateTime, DateTime exitDateTime)
        {
            var entryStartDateTime = entryDateTime.Date + new TimeSpan(6, 0, 0);
            var entryEndDateTime = entryDateTime.Date + new TimeSpan(9, 0, 0);

            var exitStartDateTime = entryDateTime.Date + new TimeSpan(15, 30, 0);
            var exitEndDateTime = entryDateTime.Date + new TimeSpan(23, 30, 0);

            if (entryDateTime.IsTimeInBetween(entryStartDateTime, entryEndDateTime) && exitDateTime.IsTimeInBetween(exitStartDateTime, exitEndDateTime))
                return _rate;

            return null;
        }
    }
}

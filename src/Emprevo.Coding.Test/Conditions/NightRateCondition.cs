﻿using System;
using Emprevo.Coding.Test.Extensions;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;

namespace Emprevo.Coding.Test.Conditions
{
    public class NightRateCondition : IRateCondition
    {
        protected readonly IRate _rate;

        public NightRateCondition(IRate rate)
        {
            _rate = rate;
            _rate.Register(this);
        }

        public IRate Condition(DateTime entryDateTime, DateTime exitDateTime)
        {
            if(!(entryDateTime.IsWeekDay() && exitDateTime.IsWeekDay()))
                return null;

            var entryStartDateTime = entryDateTime.Date + new TimeSpan(18, 0, 0);
            var entryEndDateTime = entryDateTime.Date + new TimeSpan(24, 0, 0);

            var exitEndDateTime = entryDateTime.Date.AddDays(1)+ new TimeSpan(6, 0, 0);

            if (entryDateTime.IsTimeInBetween(entryStartDateTime, entryEndDateTime) && exitDateTime.IsTimeBefore(exitEndDateTime))
                return _rate;

            return null;
        }
    }
}

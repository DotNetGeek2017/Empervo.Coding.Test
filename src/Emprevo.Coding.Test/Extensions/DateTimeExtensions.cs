﻿using System;

namespace Emprevo.Coding.Test.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsTimeInBetween(this DateTime dateTime,DateTime startDateTime,DateTime endDateTime)
        {
            return dateTime >= startDateTime && dateTime <= endDateTime; 
        }

        public static bool IsTimeBefore(this DateTime dateTime, DateTime endDateTime)
        {
            return dateTime <= endDateTime;
        }

        public static bool IsWeekend(this DateTime dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsWeekDay(this DateTime dateTime)
        {
            return !dateTime.IsWeekend();
        }

        public static bool BeforeMidNight(this DateTime dateTime)
        {
            var midNightDateTime = dateTime.Date + new TimeSpan(23, 59, 59);
            return dateTime <= midNightDateTime;
                
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;

namespace Emprevo.Coding.Test
{
    public abstract class BaseCalculateEngine : ICarParkCalculateEngine
    {
        protected readonly IEnumerable<IRateCondition> _rateConditions;

        public BaseCalculateEngine(IEnumerable<IRateCondition> rateConditions)
        {
            _rateConditions = rateConditions;
        }

        public virtual CarParkingFeeDetails GetCarParkingPriceDetails(DateTime entryDateTime,DateTime exitDateTime)
        {
            IRate selectedRate = null;

            if(exitDateTime<=entryDateTime)
                throw new Exception("Exitdate must be greater than Entrydate");

            foreach (var rateCondition in _rateConditions)
            {
                selectedRate = rateCondition.Condition(entryDateTime, exitDateTime);
                if (selectedRate != null) break;
            }

            var standardRate = new StandardRate();

            return selectedRate != null ? new CarParkingFeeDetails { Rate = selectedRate}:
                 new CarParkingFeeDetails { Rate = standardRate, TotalParkingFee = standardRate.TotalPrice(entryDateTime, exitDateTime) }; ;
        }
    }
}

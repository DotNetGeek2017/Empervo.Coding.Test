﻿using System;
using System.Linq;
using Autofac;
using Emprevo.Coding.Test.Interface;

namespace Emprevo.Coding.Test
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            if(args==null || !args.Any() || args.Count()!=2)
            {
                Console.WriteLine("Please provide the patron Entry & Exit DateTime as parameters. \r\nExample:Emprevo.Coding.Test.exe  \"2018-02-16 11:59:00.000\"  \"2018-02-16 19:19:00.000\"");
                return;
            }

            DateTime? entryDateTime=DateTime.TryParse(args[0], out var date1)? date1:(DateTime?) null;
            DateTime? exitDateTime = DateTime.TryParse(args[1], out var date2) ? date2 : (DateTime?)null;

            if(entryDateTime==null || exitDateTime == null){
                Console.WriteLine("Please provide the patron Entry & Exit DateTime as parameters. \r\n Example:Emprevo.Coding.Test.exe  \"2018-02-16 11:59:00.000\"  \"2018-02-16 9:19:00.000\"");
                return;
            }

            Container = ContainerInitialization.Configure();

            using (var scope=Container.BeginLifetimeScope())
            {
                var carParkCalculateEngine = scope.Resolve<ICarParkCalculateEngine>();
                var carparkingDetails=carParkCalculateEngine.GetCarParkingPriceDetails(entryDateTime.Value, exitDateTime.Value);

                Console.WriteLine(carparkingDetails.Rate.Name);
                Console.WriteLine(carparkingDetails.TotalParkingFee);
                Console.ReadKey();
            }
        }
    }
}

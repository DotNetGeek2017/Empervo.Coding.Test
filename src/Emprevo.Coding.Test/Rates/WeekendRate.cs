﻿using System.Collections.Generic;
using Emprevo.Coding.Test.Interface;

namespace Emprevo.Coding.Test.Rates
{
    public class WeekendRate:IRate
    {
        protected readonly List<IRateCondition> rateConditions;
        public WeekendRate()
        {
            rateConditions = new List<IRateCondition>();
        }

        public string Name { get => "Weekend Rate"; }
        public RateType Type { get => RateType.FlatRate; }
        public decimal Fee => 10.00m;

        public void Register(dynamic rateCondition)
        {
            this.rateConditions.Add(rateCondition);
        }
    }
}

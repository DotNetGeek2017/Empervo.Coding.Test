﻿using System;
using Emprevo.Coding.Test.Interface;

namespace Emprevo.Coding.Test.Rates
{
    public class StandardRate : IStandardRate
    {
        public string Name { get => "Standard Rate"; }
        public RateType Type { get => RateType.HourlyRate; }
        public decimal Fee => 5.00m;
        public decimal MaxRatePerDay => 20.00m;

        public void Register(dynamic rateCondition)
        {
            throw new NotImplementedException();
        }

        public decimal TotalPrice(DateTime entryDateTime,DateTime exitDateTime)
        {
            var result= (exitDateTime - entryDateTime);
            var totalMinutes= new TimeSpan(result.Hours, result.Minutes, result.Seconds).TotalMinutes;

            return result.Days>0? (MaxRatePerDay * result.Days)+ ComputePriceForMinutes(totalMinutes) : ComputePriceForMinutes(totalMinutes);
        }

        public decimal ComputePriceForMinutes(double minutes)
        {
            if (minutes > 0 && minutes <= 60) return Fee;

            if (minutes > 60 && minutes <= 120) return Fee * 2;

            if (minutes > 120 && minutes <= 180) return Fee * 3;

            if (minutes > 180) return MaxRatePerDay;

            return 0.00m;
        }
    }
}

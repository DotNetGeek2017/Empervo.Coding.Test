﻿using System.Collections.Generic;
using Emprevo.Coding.Test.Interface;

namespace Emprevo.Coding.Test.Rates
{
    public class EarlyBirdRate : IRate
    {
        protected readonly List<IRateCondition> rateConditions;
        public EarlyBirdRate()
        {
            rateConditions = new List<IRateCondition>();
        }

        public string Name { get => "Early Bird"; }
        public RateType Type { get => RateType.FlatRate; }
        public decimal Fee => 13.00m;

        public void Register(dynamic rateCondition)
        {
            this.rateConditions.Add(rateCondition);
        }
    }
}

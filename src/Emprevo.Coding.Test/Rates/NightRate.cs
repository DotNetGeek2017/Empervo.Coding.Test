﻿using System.Collections.Generic;
using Emprevo.Coding.Test.Interface;

namespace Emprevo.Coding.Test.Rates
{
    public class NightRate:IRate
    {
        protected readonly List<IRateCondition> rateConditions;
        public NightRate()
        {
            rateConditions = new List<IRateCondition>();
        }


        public string Name { get => "Night Rate"; }
        public RateType Type { get => RateType.FlatRate; }
        public decimal Fee => 6.50m;

        public void Register(dynamic rateCondition)
        {
            this.rateConditions.Add(rateCondition);
        }
    }
}

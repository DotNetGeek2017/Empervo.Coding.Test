﻿using Autofac;
using Emprevo.Coding.Test.Conditions;
using Emprevo.Coding.Test.Interface;
using Emprevo.Coding.Test.Rates;

namespace Emprevo.Coding.Test
{
    public static class ContainerInitialization
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            //Adding Parking Rates
            builder.RegisterType<EarlyBirdRate>().As<IRate>().AsSelf();
            builder.RegisterType<NightRate>().As<IRate>().AsSelf();
            builder.RegisterType<WeekendRate>().As<IRate>().AsSelf();

            //Adding Parking Rate Conditions
            builder.Register(c=>new EarlyBirdRateCondition(c.Resolve<EarlyBirdRate>())).As<IRateCondition>();
            builder.Register(c=>new NightRateCondition(c.Resolve<NightRate>())).As<IRateCondition>();
            builder.Register(c=>new WeekendRateCondition(c.Resolve<WeekendRate>())).As<IRateCondition>();

            //Calculate Parking engine
            builder.RegisterType<CarParkCalculateEngine>().As<ICarParkCalculateEngine>().PropertiesAutowired();
            
            return builder.Build();
        }
    }
}
